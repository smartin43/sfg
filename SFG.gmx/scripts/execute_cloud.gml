
if( plr_cloud == NULL )
{
    switch(plr_color)
    {
        case FLIP:
            if(!is_flip)
            {
                plr_cloud = create_object(x, y + flip_dir * sprite_height, plr_cloud, obj_cloud_flip);
            }
            else
            {
                plr_cloud = create_object(x, y + flip_dir * sprite_get_height(spr_cloud), plr_cloud, obj_cloud_flip);
            }
            break;
        case FLOP:
            if(!is_flip)
            {
                plr_cloud = create_object(x, y + flip_dir * sprite_height, plr_cloud, obj_cloud_flop);
            }
            else
            {
                plr_cloud = create_object(x, y + flip_dir * sprite_get_height(spr_cloud), plr_cloud, obj_cloud_flop);
            }
            break;
        default:
            break;
    }
}
else
{
    plr_cloud = destroy_object(plr_cloud);
}
