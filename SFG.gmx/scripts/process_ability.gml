var gamepad, button, ability;

gamepad = argument0;
button = argument1;
ability = argument2;


   
if( gamepad_button_check_pressed(gamepad, button) )
{
    ability_execute(ability)
}

if( gamepad_button_check_released(gamepad, button) )
{
    ability_hold(ability, false); 
}
   
if( gamepad_button_check(gamepad, button) )
{
    ability_hold(ability, true);   
}

