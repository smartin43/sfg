/* --------------------------------------------------------- Keyboard Movement -----------------------------------------------------------------------
//Tracking pressed player movement keys
if (keyboard_check_pressed(key_right) || keyboard_check_pressed(key_left))
{
    
    //Carry over cur_pressed
    if(cur_pressed)
        last_pressed = cur_pressed;
    
    //Set new cur_pressed
    if(keyboard_check_pressed(key_right))
        cur_pressed = key_right;
    else
        cur_pressed = key_left;
        
    if(on_ground == false)
    {
        state_active = false;
    }
}
//Tracking released player movement keys
if (keyboard_check_released(last_pressed))
{
    last_pressed = NULL;
}
if (keyboard_check_released(cur_pressed) && cur_pressed != NULL)
{
    if(last_pressed) 
        cur_pressed = last_pressed;
    else
        cur_pressed = NULL;  
        
    if(on_ground == false)
    {
        state_active = false;    
    }
}
*/
/*

//Applying Tracked movement keys
switch(cur_pressed)
{
    case key_right:
        //Running right
    
        //First add friction if currently running left
        if (hspd < 0)
            hspd = approach( hspd, 0, fric, -1 );
            
        hspd = approach( hspd, max_run, accel, -1 ); 
        break;
    case key_left:
        //Running left
    
        //First add friction if currently running right
        if (hspd > 0)
            hspd = approach( hspd, 0, fric, -1 );
            
        hspd = approach( hspd, -max_run, accel, -1 ); 
        break;
    default:
        //Stopping
        hspd = approach(hspd, 0, fric, -1);
    break;
}
*/
