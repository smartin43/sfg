var spd, max_spd;

spd = argument0;
max_spd = argument1;

if(abs(spd) > max_spd)
{
    spd -= sign(spd) * max_spd;
}

if(place_meeting(x - 2, y, obj_slope_slip_parent))
{
    spd = 5;
}

if(place_meeting(x + 2, y, obj_slope_slip_parent))
{
    spd = -5;
}



return spd;
