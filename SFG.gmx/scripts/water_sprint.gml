var temp_max_hspd, temp_blocks_to_run, temp_plr_multi;

switch(plr_color)
{
    case FLIP:
        temp_plr_multi = 1;
        break;
    case FLOP:
        temp_plr_multi = 2;
        break;
    default:
        temp_plr_multi = 0;
        break;
}

temp_blocks_to_run = ( temp_max_hspd / 2 - 1 ) * temp_plr_multi; 

return temp_blocks_to_run;
