var ability_value, ability_string;

ability_value = argument0;

switch(ability_value)
{
    case A_FLIP_E:
        ability_string = "Flip";
        break;
    case A_JUMP_E:
        ability_string = "Jump";
        break;
    case A_SPRINT_H:
        ability_string = "Sprint";
        break;
    case A_STATUE_T:
        ability_string = "Statue";
        break;
    case A_LIGHTNING_H:
        ability_string = "Lightning";
        break;
    case A_SWAP_E:
        ability_string = "Swap";
        break;
    case A_CLOUD_T:
        ability_string = "Cloud";
        break;
    default:
        ability_string = "EMPTY";
        break;
}

return ability_string;
