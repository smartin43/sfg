var ability

ability = argument0

switch (ability)
{
    case A_FLIP_E:
        if(on_ground)
        {
            execute_flip();
        }
        break;
    case A_JUMP_E:
        if(on_ground)
        {
            execute_jump();
        }
        break;
    case A_STATUE_T:
        execute_statue();
        break;
    case A_SWAP_E:
        execute_swap();
        break;
    case A_CLOUD_T:
        execute_cloud();
        break;
    default:
        break;
}

