var statue_object, statue_instance;

switch(plr_color)
{
    case FLIP:
        statue_object = obj_statue_flip;
        break;
    case FLOP:
        statue_object = obj_statue_flop;
        break;
    default:
        statue_object = NULL;
        break;
}

if(statue_link == false)
{
    if(!instance_exists(statue_object))
    {
        instance_create(x, y, statue_object);
    }
}
else
{
    if(instance_exists(statue_object))
    {
        statue_object.DEATH = true;
    }
}



