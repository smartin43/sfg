/*
Returns true if the object is standing on solid ground,
including one-way platforms, otherwise false.

An object is on solid ground if:
    1. There is an objFloorParent directly below it
        OR
    2.  a. There is an objOneway directly below it
            AND
        b. There is NOT an objOneway inside it
*/

x_coord = argument0;
y_coord = argument1;
color = argument2;
check_type = argument3;

var ground_state;
ground_state = false;

switch(check_type)
{
    case GROUND:
        var floor_state, oneway_state, statue_state;
        floor_state = false;
        oneway_state = false;
        statue_state = false;
        
        if(place_meeting(x_coord, y_coord, obj_ground_parent)) // Ground
        {
            if(place_meeting(x_coord, y_coord, obj_floor_parent)) // Floor
            {
                floor_state = check_color_collision(obj_floor_null_parent, obj_floor_red_parent, obj_floor_grn_parent);
            }
            
            if(place_meeting(x_coord, y_coord, obj_cloud_parent)) // Clouds
            {
                floor_state = true;
            }
        
            if(place_meeting(x_coord, y_coord, obj_oneway_parent)) // Oneway
            {
                if(flip_dir > 0) // Down Gravity
                {
                    if(place_meeting(x_coord, y_coord, obj_oneway_dn_parent) && !place_meeting(x, y, obj_oneway_dn_parent))
                    {
                        oneway_state = check_color_collision(obj_oneway_dn_null_parent, obj_oneway_dn_red_parent, obj_oneway_dn_grn_parent);
                    }
                }
                else // Up Gravity
                {
                    if(place_meeting(x_coord, y_coord, obj_oneway_up_parent) && !place_meeting(x, y, obj_oneway_up_parent))
                    {
                        oneway_state = check_color_collision(obj_oneway_up_null_parent, obj_oneway_up_red_parent, obj_oneway_up_grn_parent);
                    }
                }
            }
            
            if(place_meeting(x_coord, y_coord, obj_statue_parent) && !place_meeting(x, y, obj_statue_parent)) // Statue
            {
                statue_state = true;
            }
            
            if(floor_state || oneway_state || statue_state)
            {
                ground_state = true;
            }
        }
        else //No platform
        {
            ground_state = false;
        }
        break;
    case PLR_PUSH:
        if(place_meeting(x_coord, y_coord, obj_plr_parent))
        {
            ground_state = true;
        }
        else
        {
            ground_state = false;
        }
        break;
    default:
        ground_state = false;
        break;
}

return ground_state;
