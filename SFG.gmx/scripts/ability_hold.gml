var ability, is_held;

ability = argument0;
is_held = argument1;

switch (ability)
{
    case A_SPRINT_H:
        if(on_ground)
        {
            hold_sprint(is_held)
        }
        else
        {
            hold_sprint(false);
        }
        break;
    default:
        break;
}
