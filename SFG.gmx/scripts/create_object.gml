var object_x, object_y, object_instance, object_template;

object_x = argument0;
object_y = argument1;
object_instance = argument2;
object_template = argument3;

object_instance = instance_create(object_x, object_y, object_template);

return object_instance;
