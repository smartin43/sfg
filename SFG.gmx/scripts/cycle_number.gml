var num, max_count;

num = argument0;
max_count = argument1;

num += 1;

if(num > max_count)
    num = 0;

return num;
