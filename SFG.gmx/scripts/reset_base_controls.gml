var temp_color;

temp_color = argument0;

if(temp_color == FLIP)
{
    //Sprite holders
    /*
    neutralSprite = spr_manRed_front;
    leftSprite = spr_manRed_left;
    rightSprite = spr_manRed_right;
    fallingSprite = spr_manRed_fall;
    deathSprite = spr_manRed_death;
    */
    
    //GamePad Holders
    plr_gamepad     = 0
    key_stick       = gp_stickl
    key_stick_h      = gp_axislh
    key_stick_v      = gp_axislv
    key_button_main   = gp_shoulderl
    key_button_sub   = gp_shoulderlb
    key_button_swap_main  = gp_padu
    key_button_swap_sub   = gp_padd
    
    
    //Key holders
    key_right           = ord('D');
    key_left            = ord('A');
    key_up              = ord('W');
    key_down            = ord('S');
    key_action          = vk_space;
    key_state           = vk_lcontrol;
    
}
else//if( temp_color == FLOP )
{
    //Sprite holders
    /*
    neutralSprite = spr_manGrn_front;
    leftSprite = spr_manGrn_left;
    rightSprite = spr_manGrn_right;
    fallingSprite = spr_manGrn_fall;
    deathSprite = spr_manGrn_death;
    */
    
    
    
    //GamePad Holders
    plr_gamepad     = 0
    key_stick       = gp_stickr
    key_stick_h      = gp_axisrh
    key_stick_v      = gp_axisrv
    key_button_main   = gp_shoulderr
    key_button_sub   = gp_shoulderrb
    key_button_swap_main  = gp_face4
    key_button_swap_sub   = gp_face1
    
    //Key holders
    key_right           = vk_right;
    key_left            = vk_left;
    key_up              = vk_up;
    key_down            = vk_down;
    key_action          = vk_numpad0;
    key_state           = vk_rcontrol;
    
}

