
if( plr_statue == NULL )
{
    switch(plr_color)
    {
        case FLIP:
            plr_statue = create_object(x, y, plr_statue, obj_statue_flip);
            break;
        case FLOP:
            plr_statue = create_object(x, y, plr_statue, obj_statue_flop);
            break;
        default:
            break;
    }
}
else
{
    plr_statue = destroy_object(plr_statue);
}
