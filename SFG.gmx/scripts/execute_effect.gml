var effect_type, duration, multiplier;

effect_type = argument0;
duration = argument1;
range = argument2;
multiplier = argument3;

switch(effect_type)
{
    case SCREEN_SHAKE:
        obj_control.alarm[0] = duration;
        obj_control.screen_shake = true;
        obj_control.default_shake = range;
        obj_control.effect_multiplier = multiplier;
        break;
    default:
        break;
}
