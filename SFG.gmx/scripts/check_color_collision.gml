



var null_obj, flip_obj, flop_obj, state;
null_obj = argument0;
flip_obj = argument1;
flop_obj = argument2;

state = true;

if(!place_meeting(x_coord, y_coord, null_obj))
{
    switch(color)
    {
        case FLIP:
            if(!place_meeting(x_coord, y_coord, flip_obj) && place_meeting(x_coord, y_coord, flop_obj))
            {
                state = false;
            }
            else
            {
                state = true;
            }
            break;
        case FLOP:
            if(!place_meeting(x_coord, y_coord, flop_obj) && place_meeting(x_coord, y_coord, flip_obj))
            {
                state = false;
            }
            else
            {
                state = true;
            }
            break;
        default:
            state = true;
            break;
    }
}

return state;
